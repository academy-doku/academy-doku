package de.awacademy.abschlussprojekt.dokubackend.category;

import de.awacademy.abschlussprojekt.dokubackend.project.ProjectEntry;
import de.awacademy.abschlussprojekt.dokubackend.project.ProjectEntryDTO;
import de.awacademy.abschlussprojekt.dokubackend.project.ProjectEntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.LinkedList;
import java.util.List;

@RestController
public class CategoryController {

    @Autowired
    private CategoryEntryRepository categoryEntryRepository;
    @Autowired
    private ProjectEntryRepository projectEntryRepository;

    @GetMapping("/api/overview")
    public List<CategoryEntryDTO> showCategories() {
        List<CategoryEntryDTO> response = new LinkedList<>();
        for (CategoryEntry entry : categoryEntryRepository.findAllByOrderByCreatedAtDesc()) {
            response.add(new CategoryEntryDTO(entry.gettitle(), entry.getText(), entry.getId()));
        }
        return response;
    }

    @GetMapping("/api/category/{id}")
    public List<ProjectEntryDTO> showProjects(@PathVariable long id){
        LinkedList<ProjectEntryDTO> projectList = new LinkedList<>();

        for (ProjectEntry element: this.projectEntryRepository.findAllByCategoryEntry_Id(id)) {
            projectList.add(new ProjectEntryDTO(
                    element.getId(),
                    element.gettitle(),
                    element.getText(),
                    element.getUrl(),
                    element.getDescription(),
                    element.getTechnology()
            ));
        }
        return projectList;
    }



//    @PostMapping("/api/category/{id}")
//    public List<CategoryEntryDTO> read(@PathVariable long id) {
//        List<CategoryEntryDTO> response = new LinkedList<>();
//
//        for (CategoryEntry entry : categoryEntryRepository.findByIdOrderByCreatedAtDesc(id)) {
//            response.add(new CategoryEntryDTO(entry.gettitle(), entry.getText()));
//        }
//        return response;
//    }

//    @PostMapping("/api/category")
//    public List<CategoryEntryDTO> write(@RequestBody CategoryEntryDTO categoryEntryDTO) {
//        categoryEntryRepository.save(new CategoryEntry(categoryEntryDTO.getTitle(), categoryEntryDTO.getText()));
//        return read();
//    }

    @PostConstruct
    public void dummyData() {
        if (categoryEntryRepository.count() == 0) {
            categoryEntryRepository.save(new CategoryEntry("Game-App", "das erste Projekt"));
            categoryEntryRepository.save(new CategoryEntry("Web-App", "auf dem Weg zum Developer"));
            categoryEntryRepository.save(new CategoryEntry("Abschlussprojekt", "der letzte Streich"));
        }
    }
}
