package de.awacademy.abschlussprojekt.dokubackend;

import de.awacademy.abschlussprojekt.dokubackend.user.User;
import de.awacademy.abschlussprojekt.dokubackend.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public class SetupComponent {

    private final UserService userService;

    @Autowired
    public SetupComponent(UserService userService) {
        this.userService = userService;
    }

    @EventListener
    @Transactional
    public void handleApplicationReady(ApplicationReadyEvent event) {
        if (!userService.usernameExists("admin")) {
            User user = userService.register("admin","admin");
        }

        if (!userService.usernameExists("user")) {
            userService.register("user", "user");
        }
    }
}
