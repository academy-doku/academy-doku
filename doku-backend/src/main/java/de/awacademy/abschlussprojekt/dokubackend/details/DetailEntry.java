package de.awacademy.abschlussprojekt.dokubackend.details;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.Instant;

@Entity
public class DetailEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String description;
    private String technology;
    private Instant createdAt;


    public DetailEntry() {
    }

    public DetailEntry(String description, String technology) {
        this.description = description;
        this.technology = technology;
        this.createdAt = Instant.now();
    }

    public String getdescription() {
        return description;
    }

    public String gettechnology() {
        return technology;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public long getId() {
        return id;
    }
}
