package de.awacademy.abschlussprojekt.dokubackend.project;

import de.awacademy.abschlussprojekt.dokubackend.category.CategoryEntry;

import javax.persistence.*;
import java.time.Instant;

@Entity
public class ProjectEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String title;
    private String text;
    private String url;
    private String description;
    private String technology;
    private Instant createdAt;

    @ManyToOne
    private CategoryEntry categoryEntry;


    public ProjectEntry() {
    }

    public ProjectEntry(String title, String text, String url, CategoryEntry categoryEntry, String description, String technology) {
        this.title = title;
        this.text = text;
        this.url = url;
        this.categoryEntry = categoryEntry;
        this.description = description;
        this.technology = technology;
        this.createdAt = Instant.now();
    }

    public String gettitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public long getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public CategoryEntry getCategoryEntry() {
        return categoryEntry;
    }

    public void setCategoryEntry(CategoryEntry categoryEntry) {
        this.categoryEntry = categoryEntry;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }
}
