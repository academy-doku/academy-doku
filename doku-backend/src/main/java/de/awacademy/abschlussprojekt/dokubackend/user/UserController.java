package de.awacademy.abschlussprojekt.dokubackend.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    private UserService userService;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/api/register")
    @ResponseStatus(HttpStatus.CREATED)
    public void register(@RequestBody RegistrationDTO registrationDTO) {
        if (!this.userService.usernameExists(registrationDTO.getUsername()) &&
                registrationDTO.getPassword().equals(registrationDTO.getPassword2())) {
            userService.register(registrationDTO.getUsername(), registrationDTO.getPassword());

        }
    }
}
