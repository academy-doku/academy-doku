package de.awacademy.abschlussprojekt.dokubackend.category;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryEntryRepository extends JpaRepository<CategoryEntry, Long> {
    List<CategoryEntry> findAllByOrderByCreatedAtDesc();
    List<CategoryEntry> findByIdOrderByCreatedAtDesc(long id);

}
