package de.awacademy.abschlussprojekt.dokubackend.security;

import de.awacademy.abschlussprojekt.dokubackend.user.User;
import de.awacademy.abschlussprojekt.dokubackend.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SecurityService implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public SecurityService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsernameIgnoreCase(username)
                .orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden"));
        return new org.springframework.security.core.userdetails.User(
                user.getUsername(), user.getPassword(), List.of()
        );
    }


}

