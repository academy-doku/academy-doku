package de.awacademy.abschlussprojekt.dokubackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DokuBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(DokuBackendApplication.class, args);
	}

}
