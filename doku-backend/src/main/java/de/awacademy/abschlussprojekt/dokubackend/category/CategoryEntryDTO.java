package de.awacademy.abschlussprojekt.dokubackend.category;

public class CategoryEntryDTO {

    private String title;
    private String text;
    private long id;

    public CategoryEntryDTO(String title, String text, long id) {
        this.title = title;
        this.text = text;
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
