package de.awacademy.abschlussprojekt.dokubackend.details;

public class DetailEntryDTO {

    private long id;
    private String description;
    private String technology;

    public DetailEntryDTO(String description, String technology) {
        this.description = description;
        this.technology = technology;
    }

    public DetailEntryDTO(long id, String description, String technology) {
        this.id = id;
        this.description = description;
        this.technology = technology;
    }

    public String getdescription() {
        return description;
    }

    public void setdescription(String description) {
        this.description = description;
    }

    public String gettechnology() {
        return technology;
    }

    public void settechnology(String technology) {
        this.technology = technology;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
