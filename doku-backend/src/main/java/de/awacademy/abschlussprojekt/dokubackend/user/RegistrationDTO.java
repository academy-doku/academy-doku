package de.awacademy.abschlussprojekt.dokubackend.user;

import javax.validation.constraints.Size;

public class RegistrationDTO {
    @Size(min = 3, max = 50)
    private final String username;

    @Size(min = '@')
    private final String mail;

    @Size(min = 5)
    private final String password;
    private final String password2;

    public RegistrationDTO(String username, String mail, String password, String password2) {
        this.username = username;
        this.mail = mail;
        this.password = password;
        this.password2 = password2;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getPassword2() {
        return password2;
    }

    public String getMail() {
        return mail;
    }
}
