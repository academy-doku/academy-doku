package de.awacademy.abschlussprojekt.dokubackend.project;

import de.awacademy.abschlussprojekt.dokubackend.category.CategoryEntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

@RestController
public class ProjectController {

    @Autowired
    private ProjectEntryRepository projectEntryRepository;
    @Autowired
    private CategoryEntryRepository categoryEntryRepository;


    //    @GetMapping("/api/projects")
//    public List<ProjectEntryDTO> read() {
//        List<ProjectEntryDTO> response = new LinkedList<>();
//
//        for (ProjectEntry entry : projectEntryRepository.findAllByOrderByCreatedAtDesc()) {
//            response.add(new ProjectEntryDTO(entry.getId(), entry.gettitle(), entry.getText(), entry.getUrl()));
//        }
//        return response;
//    }
//
//
    @GetMapping("/api/details/{id}")
    public ProjectEntryDTO showProject(@PathVariable long id) {
        ProjectEntry projectEntry = projectEntryRepository.findById(id).orElseThrow();
        return new ProjectEntryDTO(projectEntry.getId(), projectEntry.gettitle(), projectEntry.getText(), projectEntry.getUrl(), projectEntry.getDescription(), projectEntry.getTechnology());
    }

    @GetMapping("/api/allprojects")
    public List<ProjectEntryDTO> showProjects() {
        List<ProjectEntryDTO> allProjects = new LinkedList<>();
        for (ProjectEntry projectEntry : projectEntryRepository.findAllByOrderByCreatedAtDesc()) {
           allProjects.add(new ProjectEntryDTO(projectEntry.getId(), projectEntry.gettitle(), projectEntry.getText(), projectEntry.getUrl(), projectEntry.getDescription(), projectEntry.getTechnology()));
        }
        return allProjects;
    }


    @GetMapping("/api/projects/{technology}")
    public List<ProjectEntryDTO> showProjectsFilter(@PathVariable String technology) {
        List<ProjectEntryDTO> allProjects = new LinkedList<>();
        for (ProjectEntry projectEntry : projectEntryRepository.findAllByTechnologyContains(technology.toLowerCase())) {
            allProjects.add(new ProjectEntryDTO(projectEntry.getId(), projectEntry.gettitle(), projectEntry.getText(), projectEntry.getUrl(), projectEntry.getDescription(), projectEntry.getTechnology()));
        }
        return allProjects;
    }


    @PostMapping("/api/category/{id}")
    public ProjectEntryDTO writeProject(@RequestBody ProjectEntryDTO ProjectEntryDTO, @PathVariable long id) {
        projectEntryRepository.save(new ProjectEntry(ProjectEntryDTO.getTitle(),
                ProjectEntryDTO.getText(), ProjectEntryDTO.getUrl(), categoryEntryRepository.findById(id).orElseThrow(), ProjectEntryDTO.getDescription(), ProjectEntryDTO.getTechnology()));

        return showProject(id);
    }

    //
//    @PostConstruct
//    public void dummyData() {
//        if (projectEntryRepository.count() == 0) {
//            CategoryEntry tempC = new CategoryEntry("Gesicht", "Ok");
//            System.out.println(">>>>>>>>" + tempC.gettitle());
//            categoryEntryRepository.save(tempC);
//            projectEntryRepository.save(new ProjectEntry("Blog KD", "Daniels und Katis Blog!", "https://gitlab.com/academy-doku/blog", tempC));
//            projectEntryRepository.save(new ProjectEntry("Pong", "Daniel Spiel!", "https://gitlab.com/academy-doku/pong", tempC));
//        }
//    }
//
    @DeleteMapping("/api/category/{id}")
    public void delete(@PathVariable long id) {
        projectEntryRepository.deleteById(id);
    }


    @PutMapping("/api/category/{id}")
    public ProjectEntryDTO update(@RequestBody ProjectEntryDTO projectEntryDTO, @PathVariable long id) {
        ProjectEntry projectEntry = projectEntryRepository.findById(id).orElseThrow();

        projectEntry.setTitle(projectEntryDTO.getTitle());
        projectEntry.setText(projectEntryDTO.getText());
        projectEntry.setUrl(projectEntryDTO.getUrl());
        projectEntry.setDescription(projectEntryDTO.getDescription());
        projectEntry.setTechnology(projectEntryDTO.getTechnology());
        projectEntryRepository.save(projectEntry);

        return new ProjectEntryDTO(
                projectEntry.getId(), projectEntry.gettitle(), projectEntry.getText(), projectEntry.getUrl(), projectEntry.getDescription(), projectEntry.getTechnology()
        );
    }

}
