import {Component, OnInit} from '@angular/core';
import {User} from '../user';
import {SecurityService} from '../security-service';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {UserRegistrationDTO} from '../UserRegistrationDTO';
import {RegistrationService} from '../registration.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  sessionUser: User | null = null;
  private registryForm: FormGroup;
  private userRegistrationDTO: UserRegistrationDTO = {
    username: '',
    password: '',
    password2: '',
  };

  constructor(private registrationService: RegistrationService) { }

  ngOnInit() {
  }

  registerUser(userRegistrationDTO: UserRegistrationDTO){
  this.registrationService.registerUser(userRegistrationDTO)
    .subscribe(r => console.log(r));
  }

  get username() {
    return this.registryForm.get('username');
  }

  get password() {
    return this.registryForm.get('password');
  }

  get password2() {
    return this.registryForm.get('password2');
  }
 /* resetForm(form?: NgForm){
    if(form != null){
    form.reset();
    this.sessionUser = {
      username: '', password1: '', password2: '', mail: '', firstName: '', lastName: '',
    };
    }

}*/
  /*OnSubmit(form: NgForm){
  this.securityService.register(form.value)
    .subscribe((data: any) =>{
    if(data.Succeeded == true){
    this.resetForm(form);
    }else
    alert("Es fehlt was!")
    });
  }*/


}
