export interface User {
  username: string;
  mail: string;
  password: string;
  password2: string;
  firstName: string;
  lastName: string;
}
