import {Component, OnInit} from '@angular/core';
import {User} from '../user';
import {SecurityService} from '../security-service';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup = new FormGroup({
    username: new FormControl(''),
     password: new FormControl(''),
  });
  sessionUser: User | null = null;
  loginData = {
    username: '', password: ''
  };

  constructor(private securityService: SecurityService) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
  }

  login() {
    this.securityService.login(this.loginData.username, this.loginData.password);
  }
}
