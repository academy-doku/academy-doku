import {Component, OnInit} from '@angular/core';
import {User} from '../user';
import {HttpClient} from '@angular/common/http';
import {SecurityService} from '../security-service';
import {Project} from '../project';
import {ActivatedRoute} from '@angular/router';
import {Category} from '../category/category';
import {ProjectserviceService} from '../projectservice.service';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {

  id: number;
  category: Category = {
    id: null,
    title: '',
    text: '',
  };
  projects: Project[] = [];
  project: Project | null = null;
  sessionUser: User | null = null;

  // newProjectEntry = {
  //   id: null,
  //   title: '',
  //   text: '',
  // };


  constructor(private projectserviceService: ProjectserviceService, private httpClient: HttpClient, private securityService: SecurityService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params.id;
    });
    console.log(this.id);
    this.httpClient.get<Project[]>('/api/category/' + this.id)
      .subscribe(entries => {
        console.log(entries);
        this.projects = entries;
      });
    this.securityService.getSessionUser()
      .subscribe(u => this.sessionUser = u);

  }


  deleteEntry(project: Project) {
    this.httpClient.delete('/api/category/' + project.id)
      .subscribe(response => {
        const index = this.projects.indexOf(project);
        this.projects.splice(index, 1);
      });
  }


}
