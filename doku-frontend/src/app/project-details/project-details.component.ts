import {Component, OnInit} from '@angular/core';
import {User} from '../user';
import {HttpClient} from '@angular/common/http';
import {SecurityService} from '../security-service';
import {ActivatedRoute} from '@angular/router';
import {Project} from '../project';
import {Location} from '@angular/common';
import {DetailEntry} from './DetailEntry';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css']
})
export class ProjectDetailsComponent implements OnInit {

  id: number;
  projectEntry: Project;
  detailEntry: DetailEntry;
  // newProjectEntry = {
  //   id: null,
  //   title: '',
  //   text: '',
  // };

  sessionUser: User | null = null;


  constructor(private route: ActivatedRoute, private securityService: SecurityService,
              private httpClient: HttpClient, private location: Location) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params.id;
      this.httpClient.get<Project>('/api/details/' + this.id).subscribe(
        e => this.projectEntry = e
      );
    });
  }

  goBack(): void {
    this.location.back();
  }
  updateEntry(project: Project) {
    this.httpClient.put<Project>('/api/category/' + project.id, this.projectEntry)
      .subscribe(response => this.projectEntry = response);
  }


}
