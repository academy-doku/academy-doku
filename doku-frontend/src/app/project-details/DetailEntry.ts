export interface DetailEntry {
  id: number;
  description: string;
  technology: string;
}
