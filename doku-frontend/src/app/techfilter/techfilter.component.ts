import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {Project} from '../project';
import {HttpClient} from '@angular/common/http';
import {Location} from '@angular/common';

@Component({
  selector: 'app-techfilter',
  templateUrl: './techfilter.component.html',
  styleUrls: ['./techfilter.component.css']
})
export class TechfilterComponent implements OnInit {

  allProjects: Project[] = [];
  technology = '';

  constructor(private route: ActivatedRoute, private http: HttpClient, private location: Location) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.technology = params.get('technology');
      this.http.get<Project[]>('/api/projects/' + this.technology)
        .subscribe(entries => {
          this.allProjects = entries;
        });
    });
  }


  goBack(): void {
    this.location.back();
  }
}
