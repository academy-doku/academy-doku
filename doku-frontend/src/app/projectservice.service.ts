import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {Project} from './project';
import {catchError, tap} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ProjectserviceService {

  categoryId: number;
  private project: Project;
  private url = 'api/allprojects'; // URL to web api
  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };


  private project$ = new BehaviorSubject<Project | null>(null);
  id: number;
  projects: Project[] = [];

  constructor(private http: HttpClient, private route: ActivatedRoute) {
    this.http.get<Project>('/api/allprojects').subscribe(
      p => this.project$.next(p)
    );
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


  searchProjects(term: string): Observable<Project[]> {
    if (!term.trim()) {
      // if not search term, return empty hero array.
      return of([]);
    }
    term = term.trim();
    // Add safe, URL encoded search parameter if there is a search term

    // const options = term ?
    //   { params: new HttpParams().set('title', term) } : {};

    return this.http.get<Project[]>(`${this.url}/?title=${term}`).pipe(
      tap(_ => console.log(`found heroes matching "${term}"`)),
      catchError(this.handleError<Project[]>('searchProjects', []))
    );


  }


}
