import {Component, OnInit} from '@angular/core';
import {Project} from '../project';
import {ProjectserviceService} from '../projectservice.service';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl} from '@angular/forms';


@Component({
  selector: 'app-project-search',
  templateUrl: './project-search.component.html',
  styleUrls: ['./project-search.component.css']
})
export class ProjectSearchComponent implements OnInit {
  myControl = new FormControl();
  options: string[] = ['Java', 'Spring', 'Angular'];
  allProjects: Project[] = [];
  filteredProjects: Project[] = [];
  term = '';
  technology = '';

  constructor(private projectserviceService: ProjectserviceService, private http: HttpClient, private route: ActivatedRoute, private router: Router) {
  }

  search(): void {
    this.filteredProjects = this.allProjects.filter(project => project.title.toLowerCase().includes(this.term.toLowerCase()));
    // this.filteredProjects = this.allProjects.includes(project,0);
  }

  ngOnInit(): void {
    this.http.get<Project[]>('/api/allprojects')
      .subscribe(entries => {
        this.allProjects = entries;
      });
  }

  // filterTech2() {
  //   this.route.paramMap.subscribe((params: ParamMap) => {
  //     this.technology = params.get('technology');
  //     this.http.get<Project[]>('/api/projects/' + this.technology)
  //       .subscribe(entries => {
  //         this.allProjects = entries;
  //       });
  //   });
  // }
}
