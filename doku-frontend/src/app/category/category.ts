export interface Category {
  id: number;
  title: string;
  text: string;
}
