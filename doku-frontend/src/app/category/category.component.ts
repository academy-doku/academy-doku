import {Component, OnInit} from '@angular/core';
import {Category} from './category';
import {HttpClient} from '@angular/common/http';
import {User} from '../user';
import {SecurityService} from '../security-service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  id: number;
  categories: Category[];
  sessionUser: User | null = null;

  newCategory = {
    title: '',
    text: '',
  };

  constructor(private httpClient: HttpClient, private securityService: SecurityService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.httpClient.get<Category[]>('/api/overview')
      .subscribe(entries => {
        console.log(entries);
        this.categories = entries;
      });
    this.securityService.getSessionUser()
      .subscribe(u => this.sessionUser = u);
  }

  writeEntry() {
    this.httpClient.post<Category[]>('/api/category', this.newCategory)
      .subscribe(entries => this.categories = entries);
    this.newCategory = {title: '', text: ''};
  }
}
