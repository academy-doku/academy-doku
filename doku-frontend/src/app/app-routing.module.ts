import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {CategoryComponent} from './category/category.component';
import {ProjectComponent} from './project/project.component';
import {FormComponent} from './form/form.component';
import {ProjectDetailsComponent} from './project-details/project-details.component';
import {ProjectSearchComponent} from './project-search/project-search.component';
import {HomeComponent} from './home/home.component';
import {TechfilterComponent} from './techfilter/techfilter.component';
import {EditComponent} from './edit/edit.component';

const routes: Routes = [
  {path: 'register', component: RegisterComponent},
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'overview', component: CategoryComponent},
  {path: 'category/:id', component: ProjectComponent},
  {path: 'category/:id/form', component: FormComponent},
  {path: 'category/:id/details/:id', component: ProjectDetailsComponent},
  {path: 'allprojects', component: ProjectSearchComponent},
  {path: 'category/details/:id', component: ProjectDetailsComponent},
  {path: 'projects/:technology', component: TechfilterComponent},
  {path: 'category/:id/details/:id/edit', component: EditComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
