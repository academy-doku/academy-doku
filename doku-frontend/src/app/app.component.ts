import {Component, OnInit} from '@angular/core';
import {SecurityService} from './security-service';
import {User} from './user';
import {FormControl} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  myControl = new FormControl();
  options: string[] = ['One', 'Two', 'Three'];
  title = 'doku-frontend';
  sessionUser: User|null = null;
  loginData = {
    username: '', password: ''
  };

  constructor(private securityService: SecurityService, private router: Router) { }
  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
  }

  logout() {
    this.securityService.logout();
    this.router.navigate(['']);
  }
}
