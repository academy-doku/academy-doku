import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Project} from '../project';
import {User} from '../user';
import {SecurityService} from '../security-service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  id: number;
  projects: Project[];
  project: Project;
  sessionUser: User | null = null;
  newProjectEntry = {
    title: '',
    text: '',
    url: '',
    description: '',
    technology: '',
  };

  constructor(private httpClient: HttpClient, private securityService: SecurityService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
  }

  writeEntry() {
    this.route.paramMap.subscribe(params => {
      this.id = +params.get('id');
    });
    this.httpClient.post<Project>('/api/category/' + this.id, this.newProjectEntry)
      .subscribe(entries => this.project = entries);
    this.newProjectEntry = {title: '', text: '', url: '', description: '', technology: ''};
    this.router.navigate(['/overview']);
  }

}
